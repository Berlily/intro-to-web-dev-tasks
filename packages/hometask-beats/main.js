import Swiper, { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/navigation";

document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("burger-menu").addEventListener("click", () => {
    document.querySelector(".header").classList.toggle("open");
  });
});

Swiper.use([Navigation]);

const swiper = new Swiper(".image-slider", {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  centeredSlides: true,
  grabCursor: true,
  loop: true,
  spaceBetween: -55,
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
  },
});
