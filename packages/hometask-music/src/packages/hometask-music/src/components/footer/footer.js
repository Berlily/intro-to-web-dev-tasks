/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */

import { getFooterListItems } from "../../api/footer-items";
import "./footer.css";

function createFooter() {
  const container = document.createElement("footer");

  container.className = "footer";

  const ul = document.createElement("ul");

  ul.className = "footer__list";

  const arr = getFooterListItems();

  arr.forEach((item) => {
    const button = document.createElement("button");

    button.className = "footer__list-buttons";

    const li = document.createElement("li");

    button.innerText = item.title;

    li.append(button);
    ul.append(li);
  });

  container.append(ul);
  return container;
}

export function setupFooter(parent) {
  const footer = createFooter();

  parent.append(footer);
  return footer;
}
